# careffour-delivery app
Une petite app: gestion de choix de livraison
Nous opter pour deux entity: Client et Delivery
On considere qu'il existe un autre microservice qui gere tout ce qui est livreurs et les livraisons effectives \
C'est pour cette raison que nou sne representons pas l'entity livreur pour mieux se concentre sur le choix du mode de livraison et du creneau.

Nos avons deux entity qui repondent parfaitement aux besoins: 
-Choix du mode livraison a travers l'attribut mode dans delivery
-EN ce qui concerne le creneau, des que le client choisit le jour, on interroge le micorservice des livreurs pour avoir les \
livreurs disponible a cette journee et ensuite on propose au client le choix d'un qui l'arrage. 
NB: On ne propose que les creneaux disponibles car certains livreurs peuvent ne pas etre diponible sur toute la journee en raison 
d'autres clients qui ont deja reserver
## Technical choice
-Java 21
-Spring boot 3.2.2, webflux
-Postgres 16

## Getting started

Clone the project
git clone https://gitlab.com/demo7655206/carrefour-delivery.git

## Run the project
-Run: docker compose up --force-recreate --build

-API Docs: http://localhost:8080/v3/api-docs or open ./redoc-static.html on brownser

-Non Blocking and stream demo
 http://localhost:8080/api/deliveries

## Tickets: Step to reproduice the app
-Choose your technologies
-Initilize the application
-Conception/Modelisation
-Create a CRUD & Add comments
-Add unit & integration tests
-Configure api-docs
-Add Spring boot HATEOAS
-Configure docker build, docker compose
-Add git lab CI/CD
-Write readme

## Next
-Add security
-Improve Dto
-Improve && Deploy via CI/CD
-Improve Spring boot HATEOAS
-Add kafka
