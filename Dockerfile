FROM maven:3.9.4-amazoncorretto-21
EXPOSE 8080
WORKDIR /
COPY . .
RUN mvn clean package spring-boot:repackage -DskipTests=true
ENTRYPOINT ["java","-jar","/target/delivery-0.0.1-SNAPSHOT.jar"]
