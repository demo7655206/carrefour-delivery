package com.carrefour.delivery.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

 class ClientTest {

    @Test
     void testClient() {
        // Create a new Client object
        Client client = Client.builder()
                .id(1L)
                .name("John Doe")
                .email("john.doe@example.com")
                .phone("1234567890")
                .address("123 Main St")
                .build();

        // Verify the values of the Client object
        Assertions.assertEquals(1L, client.getId());
        Assertions.assertEquals("John Doe", client.getName());
        Assertions.assertEquals("john.doe@example.com", client.getEmail());
        Assertions.assertEquals("1234567890", client.getPhone());
        Assertions.assertEquals("123 Main St", client.getAddress());
    }


}
