package com.carrefour.delivery.domain;

import org.junit.jupiter.api.Test;

import com.carrefour.delivery.domain.enumeration.DeliveryMode;
import com.carrefour.delivery.domain.enumeration.DeliveryStatus;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;

class DeliveryTest {

    @Test
    void testDeliveryEntity() {
        // Create a Delivery object
        Delivery delivery = Delivery.builder()
                .id(1L)
                .mode(DeliveryMode.DRIVE)
                .startTime(LocalTime.of(9, 0))
                .endTime(LocalTime.of(17, 0))
                .scheduledDate(LocalDate.of(2022, 1, 1))
                .deliveryDate(Instant.now())
                .status(DeliveryStatus.DELIVERED)
                .deliveryAddress("123 Main St")
                .clientId(1L)
                .build();

        // Verify the values of the Delivery object
        assertEquals(1L, delivery.getId());
        assertEquals(DeliveryMode.DRIVE, delivery.getMode());
        assertEquals(LocalTime.of(9, 0), delivery.getStartTime());
        assertEquals(LocalTime.of(17, 0), delivery.getEndTime());
        assertEquals(LocalDate.of(2022, 1, 1), delivery.getScheduledDate());
        assertNotNull(delivery.getDeliveryDate());
        assertEquals(DeliveryStatus.DELIVERED, delivery.getStatus());
        assertEquals("123 Main St", delivery.getDeliveryAddress());
        assertEquals(1L, delivery.getClientId());
    }
}
