package com.carrefour.delivery.web;

import com.carrefour.delivery.domain.Client;
import com.carrefour.delivery.service.ClientService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
public class ClientResourceIntTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private ClientService clientService;

    @Test
    public void getClientsTest() {
        webTestClient.get().uri("/api/clients")
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(Client.class);
    }
}
