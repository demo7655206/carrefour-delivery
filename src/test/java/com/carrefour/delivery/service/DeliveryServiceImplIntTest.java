package com.carrefour.delivery.service;

import com.carrefour.delivery.DeliveryApplication;
import com.carrefour.delivery.repository.DeliveryRepository;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.test.StepVerifier;

@SpringBootTest(classes = DeliveryApplication.class)
@AutoConfigureWebTestClient
class DeliveryServiceImplIntTest {

    @Autowired
    private DeliveryService deliveryService;


    @Test
    void testFindAll() {
        StepVerifier.create(deliveryService.findAll())
                .expectNextCount(10)
                .verifyComplete();
    }

    @Test
    void testCountAll() {
        StepVerifier.create(deliveryService.countAll())
                .expectNext(10L)
                .verifyComplete();
    }

    @Test
    void testFindOne() {
        Long id = 9L;
        StepVerifier.create(deliveryService.findOne(id))
                .expectNextMatches(delivery -> delivery.getId().equals(id))
                .verifyComplete();
    }

    @Test
    void testDelete() {
        Long id = 1L;
        StepVerifier.create(deliveryService.delete(id))
                .verifyComplete();

        // Verify the delivery was deleted
        StepVerifier.create(deliveryService.findOne(id))
                .expectNextCount(0)
                .verifyComplete();
    }
}
