package com.carrefour.delivery.service;

import com.carrefour.delivery.DeliveryApplication;
import com.carrefour.delivery.repository.ClientRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.test.StepVerifier;


@SpringBootTest(classes = DeliveryApplication.class)
@AutoConfigureWebTestClient

 class ClientServiceImplIntTest {

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientRepository clientRepository;

    @Test
     void testFindAll() {
        StepVerifier.create(clientService.findAll())
            .expectNextCount(10) // replace with expected number of clients
            .verifyComplete();
    }

    @Test
     void testCountAll() {
        StepVerifier.create(clientService.countAll())
            .expectNext(10L) // replace with expected count
            .verifyComplete();
    }

    @Test
     void testFindOne() {
        Long id = 1L; // replace with an id that exists in your database
        StepVerifier.create(clientService.findOne(id))
            .expectNextMatches(client -> client.getId().equals(id))
            .verifyComplete();
    }

    // @Test
    //  void testDelete() {
    //     Long id = 1L; // replace with an id that exists in your database
    //     StepVerifier.create(clientService.delete(id))
    //         .verifyComplete();

    //     // Verify the client was deleted
    //     StepVerifier.create(clientRepository.findById(id))
    //         .expectNextCount(0)
    //         .verifyComplete();
    // }
}
