CREATE TABLE IF NOT EXISTS client (
    id BIGINT PRIMARY KEY,
    name VARCHAR(255),
    email VARCHAR(255),
    phone VARCHAR(20),
    address VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS delivery (
    id BIGINT PRIMARY KEY,
    mode VARCHAR(50),
    start_time TIME,
    end_time TIME,
    scheduled_date DATE,
    delivery_date TIMESTAMP,
    status VARCHAR(50),
    delivery_address VARCHAR(255),
    client_id BIGINT,
    FOREIGN KEY (client_id) REFERENCES client(id)
);
