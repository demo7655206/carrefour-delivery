DELETE FROM delivery;
DELETE FROM client;

-- Inserting data into the 'client' table
INSERT INTO client (id, name, email, phone, address)
VALUES
  (1, 'John Doe', 'john@example.com', '+1234567890', '123 Main St'),
  (2, 'Jane Smith', 'jane@example.com', '+9876543210', '456 Oak Ave'),
  (3, 'Bob Johnson', 'bob@example.com', '+1122334455', '789 Pine Rd'),
  (4, 'Alice Brown', 'alice@example.com', '+9988776655', '101 Maple Ln'),
  (5, 'Charlie Wilson', 'charlie@example.com', '+6677889900', '543 Cedar St'),
  (6, 'Eva Davis', 'eva@example.com', '+1122334455', '876 Birch Rd'),
  (7, 'Frank White', 'frank@example.com', '+5544332211', '234 Elm Ave'),
  (8, 'Grace Miller', 'grace@example.com', '+4455667788', '678 Pine Rd'),
  (9, 'David Anderson', 'david@example.com', '+6677889900', '432 Oak Ave'),
  (10, 'Sophie Moore', 'sophie@example.com', '+1122334455', '876 Maple Ln');

-- Inserting data into the 'delivery' table
INSERT INTO delivery (id, mode, start_time, end_time, scheduled_date, delivery_date, status, delivery_address, client_id)
VALUES
  (1, 'DRIVE', '09:00:00', '11:00:00', '2023-05-01', '2023-05-01T10:30:00Z', 'DELIVERED', '123 Main St', 1),
  (2, 'DELIVERY', '14:00:00', '16:00:00', '2023-05-02', '2023-05-02T15:30:00Z', 'SCHEDULED', '456 Oak Ave', 2),
  (3, 'DELIVERY_TODAY', '18:00:00', '20:00:00', '2023-05-03', '2023-05-03T19:45:00Z', 'IN_PROGRESS', '789 Pine Rd', 3),
  (4, 'DELIVERY_ASAP', '10:30:00', '12:30:00', '2023-05-04', '2023-05-04T11:45:00Z', 'CANCELLED', '101 Maple Ln', 4),
  (5, 'DRIVE', '13:45:00', '15:45:00', '2023-05-05', '2023-05-05T14:30:00Z', 'DELIVERED', '543 Cedar St', 5),
  (6, 'DELIVERY', '19:30:00', '21:30:00', '2023-05-06', '2023-05-06T20:15:00Z', 'SCHEDULED', '876 Birch Rd', 6),
  (7, 'DELIVERY_TODAY', '11:00:00', '13:00:00', '2023-05-07', '2023-05-07T12:30:00Z', 'IN_PROGRESS', '234 Elm Ave', 7),
  (8, 'DELIVERY_ASAP', '15:15:00', '17:15:00', '2023-05-08', '2023-05-08T16:00:00Z', 'CANCELLED', '678 Pine Rd', 8),
  (9, 'DRIVE', '20:00:00', '22:00:00', '2023-05-09', '2023-05-09T21:30:00Z', 'DELIVERED', '432 Oak Ave', 9),
  (10, 'DELIVERY', '12:30:00', '14:30:00', '2023-05-10', '2023-05-10T13:45:00Z', 'SCHEDULED', '876 Maple Ln', 10);
