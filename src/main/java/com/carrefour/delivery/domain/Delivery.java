package com.carrefour.delivery.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import com.carrefour.delivery.domain.enumeration.DeliveryMode;
import com.carrefour.delivery.domain.enumeration.DeliveryStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.time.LocalTime;

/**
 * A Delivery.
 */
@Table("delivery")
@Builder
@AllArgsConstructor
@Data
public class Delivery implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("mode")
    private DeliveryMode mode;

    @Column("start_time")
    private LocalTime startTime;

    @Column("end_time")
    private LocalTime endTime;

    @Column("scheduled_date")
    private LocalDate scheduledDate;

    @Column("delivery_date")
    private Instant deliveryDate;

    @Column("status")
    private DeliveryStatus status;

    @Column("delivery_address")
    private String deliveryAddress;

    // @Transient
    // private Client client;

    @Column("client_id")
    private Long clientId;


}
