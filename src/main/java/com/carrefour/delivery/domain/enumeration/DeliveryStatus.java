package com.carrefour.delivery.domain.enumeration;

/**
 * The DeliveryStatus enumeration.
 */
public enum DeliveryStatus {
    SCHEDULED,
    IN_PROGRESS,
    DELIVERED,
    CANCELLED,
}
