package com.carrefour.delivery.domain.enumeration;

/**
 * The DeliveryMode enumeration.
 */
public enum DeliveryMode {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP,
}
