package com.carrefour.delivery.service;

import com.carrefour.delivery.domain.Delivery;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface DeliveryService {

    Mono<Delivery> save(Delivery delivery);

    Mono<Delivery> update(Delivery delivery);
    Flux<Delivery> findAll();


    Mono<Long> countAll();

    Mono<Delivery> findOne(Long id);

    Mono<Void> delete(Long id);
}
