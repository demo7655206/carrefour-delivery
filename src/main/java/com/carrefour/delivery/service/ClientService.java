package com.carrefour.delivery.service;

import com.carrefour.delivery.domain.Client;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ClientService {

    Mono<Client> save(Client client);
    Mono<Client> update(Client client);
    Flux<Client> findAll();
    Mono<Long> countAll();
    Mono<Client> findOne(Long id);
    Mono<Void> delete(Long id);
}
