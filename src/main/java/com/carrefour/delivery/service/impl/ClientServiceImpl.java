package com.carrefour.delivery.service.impl;


import java.time.Duration;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carrefour.delivery.domain.Client;
import com.carrefour.delivery.repository.ClientRepository;
import com.carrefour.delivery.service.ClientService;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
@Slf4j
public class ClientServiceImpl implements ClientService {


    private final ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Mono<Client> save(Client client) {
        log.debug("Request to save Client : {}", client);
        return clientRepository.save(client);
    }

    @Override
    public Mono<Client> update(Client client) {
        log.debug("Request to update Client : {}", client);
        return clientRepository.save(client);
    }



    @Override
    public Flux<Client> findAll() {
        log.debug("Request to get all Clients");
        return clientRepository.findAll().delayElements(Duration.ofMillis(100));
    }

    public Mono<Long> countAll() {
        return clientRepository.count();
    }

    @Override
    @Transactional(readOnly = true)
    public Mono<Client> findOne(Long id) {
        log.debug("Request to get Client : {}", id);
        return clientRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Client : {}", id);
        return clientRepository.deleteById(id);
    }
}
