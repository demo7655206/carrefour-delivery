package com.carrefour.delivery.service.impl;

import java.time.Duration;

import org.springframework.stereotype.Service;

import com.carrefour.delivery.domain.Delivery;
import com.carrefour.delivery.repository.DeliveryRepository;
import com.carrefour.delivery.service.DeliveryService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@Slf4j
public class DeliveryServiceImpl implements DeliveryService {

    private final DeliveryRepository deliveryRepository;

    @Override
    public Mono<Delivery> save(Delivery delivery) {
        log.debug("Request to save Delivery : {}", delivery);
        return deliveryRepository.save(delivery);
    }

    @Override
    public Mono<Delivery> update(Delivery delivery) {
        log.debug("Request to update Delivery : {}", delivery);
        return deliveryRepository.save(delivery);
    }


    @Override
    public Flux<Delivery> findAll() {
        log.debug("Request to get all Deliveries");
        return deliveryRepository.findAll().delayElements(Duration.ofMillis(1000));
    }

    @Override
    public Mono<Long> countAll() {
        return deliveryRepository.count();
    }

    @Override
    public Mono<Delivery> findOne(Long id) {
        log.debug("Request to get Delivery : {}", id);
        return deliveryRepository.findById(id);
    }

    @Override
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete Delivery : {}", id);
        return deliveryRepository.deleteById(id);
    }
}
