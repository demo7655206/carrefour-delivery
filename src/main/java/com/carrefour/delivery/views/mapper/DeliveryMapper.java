package com.carrefour.delivery.views.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.carrefour.delivery.domain.Delivery;
import com.carrefour.delivery.views.DeliveryDTO;

@Mapper(componentModel = "spring")
public interface DeliveryMapper {
    DeliveryMapper INSTANCE = Mappers.getMapper(DeliveryMapper.class);

    // @Mapping(source = "client", target = "client")
    // @Mapping(target = "links", source = "links")
    DeliveryDTO toDto(Delivery delivery);

}
