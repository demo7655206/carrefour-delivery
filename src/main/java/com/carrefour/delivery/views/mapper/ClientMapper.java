package com.carrefour.delivery.views.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.carrefour.delivery.domain.Client;
import com.carrefour.delivery.views.ClientDTO;

@Mapper(componentModel = "spring")
public interface ClientMapper {
    ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);

    ClientDTO clientToClientDTO(Client client);
}
