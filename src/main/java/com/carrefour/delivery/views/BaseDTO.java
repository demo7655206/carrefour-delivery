package com.carrefour.delivery.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.Link;


public abstract class BaseDTO<T extends BaseDTO<T>>  implements Serializable {

    protected List<Link> links = new ArrayList<>();

    public T addLink(Link link) {
        links.add(link);
        return (T)this;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

}


