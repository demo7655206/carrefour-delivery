package com.carrefour.delivery.views;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.Link;

import com.carrefour.delivery.domain.enumeration.DeliveryMode;
import com.carrefour.delivery.domain.enumeration.DeliveryStatus;
import lombok.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryDTO extends BaseDTO<DeliveryDTO> {

    private Long id;
    private DeliveryMode mode;
    private LocalTime startTime;
    private LocalTime endTime;
    private LocalDate scheduledDate;
    private Instant deliveryDate;
    private DeliveryStatus status;
    private String deliveryAddress;
    private Long clientId;
    List<Link> links = new ArrayList<>();

}


