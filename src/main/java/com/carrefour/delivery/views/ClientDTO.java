package com.carrefour.delivery.views;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClientDTO extends BaseDTO<DeliveryDTO>{
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String address;
}


