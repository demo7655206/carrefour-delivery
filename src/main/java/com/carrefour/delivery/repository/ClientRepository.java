package com.carrefour.delivery.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.carrefour.delivery.domain.Client;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data R2DBC repository for the Client entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientRepository extends ReactiveCrudRepository<Client, Long> {

}


