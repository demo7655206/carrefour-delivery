package com.carrefour.delivery.web.rest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import com.carrefour.delivery.domain.Delivery;
import com.carrefour.delivery.repository.DeliveryRepository;
import com.carrefour.delivery.service.DeliveryService;
import com.carrefour.delivery.views.DeliveryDTO;
import com.carrefour.delivery.views.mapper.DeliveryMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/deliveries")
@RequiredArgsConstructor
@Slf4j
public class DeliveryResource {

    private final DeliveryMapper deliveryMapper;

    private final DeliveryService deliveryService;

    private final DeliveryRepository deliveryRepository;



    /**
     * {@code POST  /deliveries} : Create a new delivery.
     *
     * @param delivery the delivery to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new delivery, or with status {@code 400 (Bad Request)} if the delivery has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public Mono<ResponseEntity<Delivery>> createDelivery(@RequestBody Delivery delivery) throws URISyntaxException {
        log.debug("REST request to save Delivery : {}", delivery);

        return deliveryService
            .save(delivery)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/deliveries/" + result.getId()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /deliveries/:id} : Updates an existing delivery.
     *
     * @param id the id of the delivery to save.
     * @param delivery the delivery to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated delivery,
     * or with status {@code 400 (Bad Request)} if the delivery is not valid,
     * or with status {@code 500 (Internal Server Error)} if the delivery couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public Mono<ResponseEntity<Delivery>> updateDelivery(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Delivery delivery
    ) throws URISyntaxException {
        log.debug("REST request to update Delivery : {}, {}", id, delivery);


        return deliveryRepository
            .existsById(id)
            .flatMap(exists -> {

                return deliveryService
                    .update(delivery)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .body(result)
                    );
            });
    }




    /**
     * {@code GET  /deliveries} : get all the deliveries as a stream.
     * @return the {@link Flux} of deliveries.
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<DeliveryDTO> getAllDeliveriesAsStream() {
        log.debug("REST request to get all Deliveries as a stream");
        return deliveryService.findAll().map(d->{
            DeliveryDTO dto = this.toDto(d);
            Link selfLink = linkTo(DeliveryResource.class).slash(d.getId()).withSelfRel();
            Link clientLink = linkTo(methodOn(ClientResource.class).getClient(d.getClientId())).withRel("client");
            dto.addLink(selfLink);
            dto.addLink(clientLink);
            log.debug("Delivery DTO : {}", dto);
            return dto;
        });
    }

    /**
     * {@code GET  /deliveries/:id} : get the "id" delivery.
     *
     * @param id the id of the delivery to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the delivery, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public Mono<ResponseEntity<Delivery>> getDelivery(@PathVariable("id") Long id) {
        log.debug("REST request to get Delivery : {}", id);
        Mono<Delivery> delivery = deliveryService.findOne(id);
        return delivery.flatMap(d-> Mono.just(ResponseEntity.ok().body(d)));
    }

    /**
     * {@code DELETE  /deliveries/:id} : delete the "id" delivery.
     *
     * @param id the id of the delivery to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteDelivery(@PathVariable("id") Long id) {
        log.debug("REST request to delete Delivery : {}", id);
        return deliveryService
            .delete(id)
            .then(
                Mono.just(
                    ResponseEntity
                        .noContent()
                        .build()
                )
            );
    }

    private DeliveryDTO toDto(Delivery delivery) {
        return DeliveryDTO.builder()
            .id(delivery.getId())
            .deliveryAddress(delivery.getDeliveryAddress())
            .deliveryDate(delivery.getDeliveryDate())
            .startTime(delivery.getStartTime())
            .endTime(delivery.getEndTime())
            .status(delivery.getStatus())
            .scheduledDate(delivery.getScheduledDate())
            .clientId(delivery.getClientId())
            .build();
    }
}
