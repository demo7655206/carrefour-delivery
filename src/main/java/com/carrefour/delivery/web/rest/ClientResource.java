package com.carrefour.delivery.web.rest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import com.carrefour.delivery.domain.Client;
import com.carrefour.delivery.repository.ClientRepository;
import com.carrefour.delivery.service.ClientService;
import com.carrefour.delivery.views.ClientDTO;
import com.carrefour.delivery.views.DeliveryDTO;
import com.carrefour.delivery.views.mapper.ClientMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/clients")
@RequiredArgsConstructor
@Slf4j
public class ClientResource {

    private final ClientService clientService;

    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;

    /**
     * {@code POST  /clients} : Create a new client.
     *
     * @param client the client to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new client, or with status {@code 400 (Bad Request)} if the
     *         client has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public Mono<ResponseEntity<Client>> createClient(@RequestBody Client client) throws URISyntaxException {
        log.debug("REST request to save Client : {}", client);
        if (client.getId() == null) {

            return clientService
                    .save(client)
                    .map(result -> {
                        try {
                            return ResponseEntity
                                    .created(new URI("/api/clients/" + result.getId()))
                                    .body(result);
                        } catch (URISyntaxException e) {
                            throw new RuntimeException(e);
                        }
                    });
        }
        return null;
    }

    /**
     * {@code PUT  /clients/:id} : Updates an existing client.
     *
     * @param id     the id of the client to save.
     * @param client the client to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated client,
     *         or with status {@code 400 (Bad Request)} if the client is not valid,
     *         or with status {@code 500 (Internal Server Error)} if the client
     *         couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public Mono<ResponseEntity<Client>> updateClient(
            @PathVariable(value = "id", required = false) final Long id,
            @RequestBody Client client) throws URISyntaxException {
        log.debug("REST request to update Client : {}, {}", id, client);
        if (client.getId() != null && Objects.equals(id, client.getId())) {

            return clientRepository
                    .existsById(id)
                    .flatMap(exists -> {
                        return clientService
                                .update(client)
                                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                                .map(result -> ResponseEntity
                                        .ok()
                                        .body(result));
                    });

        }

        return null;
    }

    /**
     * {@code GET  /clients} : get all the clients as a stream.
     *
     * @return the {@link Flux} of clients.
     */
    @GetMapping(value = "")
    public Flux<ClientDTO> getAllClientsAsStream() {
        log.debug("REST request to get all Clients as a stream");
        return clientService.findAll().map(d->{
            ClientDTO dto = toDto(d);
            Link selfLink = linkTo(ClientResource.class).slash(d.getId()).withSelfRel();
            dto.addLink(selfLink);
            return dto;
        });
    }

    /**
     * {@code GET  /clients/:id} : get the "id" client.
     *
     * @param id the id of the client to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the client, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public Mono<ResponseEntity<Client>> getClient(@PathVariable("id") Long id) {
        log.debug("REST request to get Client : {}", id);
        Mono<Client> client = clientService.findOne(id);
        return client.flatMap(c -> Mono.just(ResponseEntity.ok().body(c)));
    }

    /**
     * {@code DELETE  /clients/:id} : delete the "id" client.
     *
     * @param id the id of the client to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteClient(@PathVariable("id") Long id) {
        log.debug("REST request to delete Client : {}", id);
        return clientService
                .delete(id)
                .then(
                        Mono.just(
                                ResponseEntity
                                        .noContent()

                                        .build()));
    }

    ClientDTO toDto(Client d) {
        return ClientDTO.builder()
                .id(d.getId())
                .name(d.getName())
                .address(d.getAddress())
                .email(d.getEmail())
                .phone(d.getPhone())
                .build();
    }
}
